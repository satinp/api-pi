# AWS Serverless template

Template used from https://github.com/fabiorogeriosj/aws-serverless-template-mysql

## How to use

create an `.env` file in the project root with the MongoDB database settings, for example:

```
MYSQL_HOST=localhost
MYSQL_USER=root
MYSQL_PASSWORD= 
MYSQL_PORT= 3306
MYSQL_DB=mydb

MYSQL_HOST_TEST=localhost
MYSQL_USER_TEST=root
MYSQL_PASSWORD_TEST= 
MYSQL_PORT_TEST= 3306
MYSQL_DB_TEST=mydbtest
```
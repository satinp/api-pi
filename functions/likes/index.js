const get = require('./likes_get.js.js')
const post = require('./likes_post.js.js')
const remove = require('./likes_remove.js.js')

module.exports = {
  get,
  post,
  remove
}

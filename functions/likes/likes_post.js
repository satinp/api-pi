const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.datahora) return util.bind(new Error('Data error!'))
    if (!body.id_user) return util.bind(new Error('User id error!'))
    if (!body.id_post) return util.bind(new Error('Post id error!'))

    const insert = await mysql.query('insert into likes (datahora, id_user, id_post) values (?,?,?)', 
    [body.datahora, body.id_user, body.id_post])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}

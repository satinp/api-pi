const get = require('./comments_get.js.js')
const post = require('./comments_post.js.js')
const put = require('./comments_put.js.js')
const remove = require('./comments_remove.js.js')

module.exports = {
  get,
  post,
  put,
  remove
}

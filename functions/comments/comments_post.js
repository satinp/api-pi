const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.comment) return util.bind(new Error('Enter your comment!'))
    if (!body.datahora) return util.bind(new Error('Enter datahora!'))
    if (!body.id_user) return util.bind(new Error('Enter user id!'))
    if (!body.id_post) return util.bind(new Error('Enter post id!'))

    const checkCommentExist = await mysql.query('select * from comments where comment=?', [body.comment])
    if (checkCommentExist.length) return util.bind(new Error('comment error!'))

    const insert = await mysql.query('insert into comments (comment, datahora, id_user, id_post) values (?,?,?,?)', 
    [body.comment, body.datahora, body.id_user, body.id_post])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}

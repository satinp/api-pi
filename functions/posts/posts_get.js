const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.id) {
      const post = await mysql.query('select id, titulo, image, datahora, id_user from posts where id_user=?', 
      [event.pathParameters.id_user])
      return util.bind(post.length ? post[0] : {})
    }

    const posts = await mysql.query('select id, titulo, image, datahora, id_user from posts')
    return util.bind(posts)
  } catch (error) {
    return util.bind(error)
  }
}

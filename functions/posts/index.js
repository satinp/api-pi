const get = require('./posts_get.js/index.js')
const post = require('./posts_post.js/index.js')
const remove = require('./posts_remove.js/index.js')

module.exports = {
  get,
  post,
  remove
}

const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.titulo) return util.bind(new Error('Titulo Error!'))
    if (!body.imagem) return util.bind(new Error('Imagem error!'))
    if (!body.datahora) return util.bind(new Error('Data herror!'))
    if (!body.id_user) return util.bind(new Error('User error!'))

    const insert = await mysql.query('insert into posts (titulo, image, datahora, id_user) values (?,?,?,?)', 
    [body.titulo, body.imagem, body.datahora, body.id_user])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}

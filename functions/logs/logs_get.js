const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.id) {
      const log = await mysql.query('select id, log, datahora, id_user from logs where id=?', [event.pathParameters.id])
      return util.bind(log.length ? log[0] : {})
    }

    const logs = await mysql.query('select id, log, datahora, id_user from logs')
    return util.bind(logs)
  } catch (error) {
    return util.bind(error)
  }
}

const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.log) return util.bind(new Error('Log error!'))
    if (!body.datahora) return util.bind(new Error('Data error!'))
    if (!body.id_user) return util.bind(new Error('User error!'))

    const insert = await mysql.query('insert into logs (log, datahora, id_user) values (?,?,?)', 
    [body.log, body.datahora, body.id_user])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
